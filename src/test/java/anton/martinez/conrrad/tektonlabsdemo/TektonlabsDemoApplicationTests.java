package anton.martinez.conrrad.tektonlabsdemo;

import anton.martinez.conrrad.tektonlabsdemo.product.domain.Product;
import anton.martinez.conrrad.tektonlabsdemo.product.domain.ProductException;
import anton.martinez.conrrad.tektonlabsdemo.product.domain.repository.QueryProductRepository;
import anton.martinez.conrrad.tektonlabsdemo.product.domain.repository.UpdateProductCommandRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class TektonlabsDemoApplicationTests {

    @Autowired
    QueryProductRepository queryProductRepository;

    @Autowired
    UpdateProductCommandRepository updateProductCommandRepository;

    @Test
    void cuandoBuscoProductoEntoncesObtengoPrecio() {
        assertEquals(new BigDecimal("3.42"), queryProductRepository.findById(1).getPrice());
    }

    @Test
    void cuandoActualizoProductoEntoncesRetornaUnaFilaAfectada() {
        final Product byId = queryProductRepository.findById(1);
        Product.ProductBuilder productBuilder = new Product.ProductBuilder();
        final Product updatedProduct = productBuilder.id(byId.getId()).name(byId.getName())
                .price(new BigDecimal(3.52)).categoryId(byId.getCategoryId()).build();
        assertEquals(1, updateProductCommandRepository.update(updatedProduct));
    }

    @Test
    void cuandoIntentoNulearPrecioEntoncesRetornaError() {
        Product.ProductBuilder productBuilder = new Product.ProductBuilder();
        final Executable executable = () -> productBuilder.price(null);
        assertThrows(ProductException.class, executable);
    }

}
