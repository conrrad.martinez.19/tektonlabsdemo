package anton.martinez.conrrad.tektonlabsdemo;

import anton.martinez.conrrad.tektonlabsdemo.product.domain.Product;
import anton.martinez.conrrad.tektonlabsdemo.product.domain.repository.QueryProductRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.FluxExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class IntegrationTest {

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private QueryProductRepository queryProductRepository;

    @Test
    void cuandoBuscoProductoEntoncesObtengoNombre() {
        final FluxExchangeResult<Product> responseEntityMono = webTestClient.get().uri("/product/id/{idProduct}", 1)
                .exchange().returnResult(Product.class);
        assertEquals("Arroz", responseEntityMono.getResponseBody().blockFirst().getName());
    }

    @Test
    void cuandoRegistroProductoEntoncesRetornaOk() {
        final Product.ProductBuilder productBuilder = new Product.ProductBuilder();
        productBuilder.categoryId(1).price(new BigDecimal("1.03")).name("Product 1").id(100);
        webTestClient.post().uri("/product").contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(productBuilder.build())).exchange().expectStatus().isNoContent();
    }

    @Test
    void cuandoActualizoProductoEntoncesRetornaOk() {
        final Product byId = queryProductRepository.findById(1);
        Product.ProductBuilder productBuilder = new Product.ProductBuilder();
        final Product updatedProduct = productBuilder.id(byId.getId()).name(byId.getName())
                .price(new BigDecimal(3.52)).categoryId(byId.getCategoryId()).build();
        webTestClient.put().uri("/product").contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(updatedProduct)).exchange().expectStatus().isOk();
    }

    @Test
    void cuandoListoCategoriasEntoncesRetornaOk() {
        webTestClient.get().uri("/category").exchange().expectStatus().isOk();
    }

    @Test
    void cuandoListoProductosPorCategoriaEntoncesRetornaOk() {
        webTestClient.get().uri("/category/{id}/products", 1).exchange().expectStatus().isOk();
    }

    @Test
    void cuandoConsultoCategoriaEntoncesRetornaOk() {
        webTestClient.get().uri("/category/{id}", 1).exchange().expectStatus().isOk();
    }



}
