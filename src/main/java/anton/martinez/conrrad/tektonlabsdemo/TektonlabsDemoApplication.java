package anton.martinez.conrrad.tektonlabsdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TektonlabsDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TektonlabsDemoApplication.class, args);
	}

}
