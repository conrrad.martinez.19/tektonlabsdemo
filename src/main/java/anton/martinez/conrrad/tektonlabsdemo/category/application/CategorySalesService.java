package anton.martinez.conrrad.tektonlabsdemo.category.application;

import anton.martinez.conrrad.tektonlabsdemo.category.domain.Category;
import anton.martinez.conrrad.tektonlabsdemo.category.domain.CategoryProductsAggregate;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class CategorySalesService {

    private CategorySalesClient client;

    public CategorySalesService(CategorySalesClient client) {
        this.client = client;
    }

    public Flux<CategoryProductsAggregate> addSalesToCategory(
            CategoryProductsAggregate categoryProductsAggregate) {
        final CategoryProductsAggregate.CategoryProductsAggregateBuilder builder = new CategoryProductsAggregate
                .CategoryProductsAggregateBuilder();
        builder.productos(categoryProductsAggregate.getProducts());
        return client.getCategorySales().filter(c -> categoryProductsAggregate.getCategory().getId().equals(
                        c.getId()))
                .flatMap(item -> Mono.just(
                        builder
                                .category(new Category.CategoryBuilder().copyOf(categoryProductsAggregate.getCategory())
                                        .sales(item.getSales()).build())
                                .build()));
    }
}
