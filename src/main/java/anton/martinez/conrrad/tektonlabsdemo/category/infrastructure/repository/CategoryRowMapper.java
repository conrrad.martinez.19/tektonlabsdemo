package anton.martinez.conrrad.tektonlabsdemo.category.infrastructure.repository;

import anton.martinez.conrrad.tektonlabsdemo.category.domain.Category;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class CategoryRowMapper implements RowMapper<Category> {
    @Override
    public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Category.CategoryBuilder().id(rs.getInt("id")).name(rs.getString("name"))
                .description(rs.getString("description")).build();
    }
}
