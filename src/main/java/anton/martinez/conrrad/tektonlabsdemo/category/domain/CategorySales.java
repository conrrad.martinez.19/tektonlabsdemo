package anton.martinez.conrrad.tektonlabsdemo.category.domain;

import java.math.BigDecimal;

public class CategorySales {

    private Integer id;
    private BigDecimal sales;

    public Integer getId() {
        return id;
    }

    public BigDecimal getSales() {
        return sales;
    }
}
