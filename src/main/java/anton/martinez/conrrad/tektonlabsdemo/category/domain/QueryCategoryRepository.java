package anton.martinez.conrrad.tektonlabsdemo.category.domain;

import java.util.List;

public interface QueryCategoryRepository {

    List<Category> findAll();
}
