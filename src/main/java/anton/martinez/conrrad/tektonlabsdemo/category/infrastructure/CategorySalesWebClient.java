package anton.martinez.conrrad.tektonlabsdemo.category.infrastructure;

import anton.martinez.conrrad.tektonlabsdemo.category.application.CategorySalesClient;
import anton.martinez.conrrad.tektonlabsdemo.category.domain.CategorySales;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@Component
public class CategorySalesWebClient implements CategorySalesClient {

    @Value("${category.baseUrl}")
    private String baseUrl;

    @Override
    public Flux<CategorySales> getCategorySales() {
        return WebClient.builder().baseUrl(baseUrl).build().get()
                .retrieve().bodyToFlux(CategorySales.class);
    }
}
