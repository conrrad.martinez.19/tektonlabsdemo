package anton.martinez.conrrad.tektonlabsdemo.category.domain;

public interface QueryCategoryIdRepository {

    Category findById(Integer id);
}
