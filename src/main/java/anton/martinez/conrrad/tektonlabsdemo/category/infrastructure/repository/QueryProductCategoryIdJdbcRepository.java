package anton.martinez.conrrad.tektonlabsdemo.category.infrastructure.repository;

import anton.martinez.conrrad.tektonlabsdemo.category.domain.QueryProductCategoryIdRepository;
import anton.martinez.conrrad.tektonlabsdemo.product.domain.Product;
import anton.martinez.conrrad.tektonlabsdemo.product.infrastructure.repository.ProductRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.List;

@Repository
public class QueryProductCategoryIdJdbcRepository implements QueryProductCategoryIdRepository {
    private JdbcTemplate jdbcTemplate;
    private ProductRowMapper productRowMapper;

    public QueryProductCategoryIdJdbcRepository(JdbcTemplate jdbcTemplate, ProductRowMapper productRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.productRowMapper = productRowMapper;
    }

    @Override
    public List<Product> findByCategoryId(Integer categoryId) {
        return jdbcTemplate.query("select * from product where categoryId = ?", new Object[]{categoryId},
                new int[]{Types.NUMERIC}, productRowMapper);
    }
}
