package anton.martinez.conrrad.tektonlabsdemo.category.domain;

import anton.martinez.conrrad.tektonlabsdemo.product.domain.Product;

import java.util.List;

public interface QueryProductCategoryIdRepository {

    List<Product> findByCategoryId(Integer categoryId);
}
