package anton.martinez.conrrad.tektonlabsdemo.category.domain;

public class CategoryProductsAggregateFactory {

    private QueryProductCategoryIdRepository queryProductCategoryIdRepository;
    private QueryCategoryIdRepository queryCategoryIdRepository;

    public CategoryProductsAggregateFactory(QueryProductCategoryIdRepository queryProductCategoryIdRepository, QueryCategoryIdRepository queryCategoryIdRepository) {
        this.queryProductCategoryIdRepository = queryProductCategoryIdRepository;
        this.queryCategoryIdRepository = queryCategoryIdRepository;
    }

    public CategoryProductsAggregate newInstance(Integer categoryId) {
        final CategoryProductsAggregate.CategoryProductsAggregateBuilder builder = new CategoryProductsAggregate.CategoryProductsAggregateBuilder();
        return builder.category(queryCategoryIdRepository.findById(categoryId))
                .productos(queryProductCategoryIdRepository.findByCategoryId(categoryId))
                .build();
    }
}
