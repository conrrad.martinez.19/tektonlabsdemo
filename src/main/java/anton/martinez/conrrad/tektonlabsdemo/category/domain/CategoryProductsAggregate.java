package anton.martinez.conrrad.tektonlabsdemo.category.domain;

import anton.martinez.conrrad.tektonlabsdemo.product.domain.Product;

import java.util.List;

public class CategoryProductsAggregate {

    private Category category;
    private List<Product> products;

    public Category getCategory() {
        return category;
    }

    public List<Product> getProducts() {
        return products;
    }

    public static class CategoryProductsAggregateBuilder {
        private CategoryProductsAggregate wrapper;

        public CategoryProductsAggregateBuilder() {
            wrapper = new CategoryProductsAggregate();
        }

        public CategoryProductsAggregateBuilder category(Category category) {
            wrapper.category = category;
            return this;
        }

        public CategoryProductsAggregateBuilder productos(List<Product> products) {
            wrapper.products = products;
            return this;
        }

        public CategoryProductsAggregate build() {
            return wrapper;
        }
    }
}
