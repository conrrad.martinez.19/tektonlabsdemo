package anton.martinez.conrrad.tektonlabsdemo.category.application;

import anton.martinez.conrrad.tektonlabsdemo.category.domain.CategorySales;
import reactor.core.publisher.Flux;

public interface CategorySalesClient {

    Flux<CategorySales> getCategorySales();
}
