package anton.martinez.conrrad.tektonlabsdemo.category.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class Category implements Serializable {

    private Integer id;
    private String name;
    private String description;
    private BigDecimal sales;

    public Category() {
    }

    public Category(Category category) {
        this.id = category.getId();
        this.description = category.getDescription();
        this.name = category.getName();
        this.sales = category.getSales();
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getSales() {
        return sales;
    }

    public static class CategoryBuilder {
        private Category wrapper;

        public CategoryBuilder() {
            this.wrapper = new Category();
        }
        public CategoryBuilder id(Integer id) {
            this.wrapper.id = id;
            return this;
        }

        public CategoryBuilder name(String name) {
            wrapper.name = name;
            return this;
        }

        public CategoryBuilder description(String description) {
            wrapper.description = description;
            return this;
        }

        public CategoryBuilder sales(BigDecimal sales) {
            wrapper.sales = sales;
            return this;
        }

        public CategoryBuilder copyOf(Category category) {
            wrapper = new Category(category);
            return this;
        }

        public Category build() {
            return wrapper;
        }
    }
}
