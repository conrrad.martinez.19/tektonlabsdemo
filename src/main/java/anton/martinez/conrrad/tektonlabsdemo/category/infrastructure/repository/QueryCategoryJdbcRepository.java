package anton.martinez.conrrad.tektonlabsdemo.category.infrastructure.repository;

import anton.martinez.conrrad.tektonlabsdemo.category.domain.Category;
import anton.martinez.conrrad.tektonlabsdemo.category.domain.QueryCategoryRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QueryCategoryJdbcRepository implements QueryCategoryRepository {

    private JdbcTemplate jdbcTemplate;
    private CategoryRowMapper categoryRowMapper;

    public QueryCategoryJdbcRepository(JdbcTemplate jdbcTemplate, CategoryRowMapper categoryRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.categoryRowMapper = categoryRowMapper;
    }

    @Cacheable
    @Override
    public List<Category> findAll() {
        return jdbcTemplate.query("select * from category", categoryRowMapper);
    }
}
