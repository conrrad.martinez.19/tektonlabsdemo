package anton.martinez.conrrad.tektonlabsdemo.category.infrastructure;

import anton.martinez.conrrad.tektonlabsdemo.category.application.CategorySalesClient;
import anton.martinez.conrrad.tektonlabsdemo.category.application.CategorySalesService;
import anton.martinez.conrrad.tektonlabsdemo.category.domain.CategoryProductsAggregateFactory;
import anton.martinez.conrrad.tektonlabsdemo.category.domain.QueryCategoryIdRepository;
import anton.martinez.conrrad.tektonlabsdemo.category.domain.QueryProductCategoryIdRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {

    @Bean
    CategoryProductsAggregateFactory categoryProductsAggregateFactory(
            QueryProductCategoryIdRepository queryProductCategoryIdRepository,
            QueryCategoryIdRepository queryCategoryIdRepository) {
        return new CategoryProductsAggregateFactory(queryProductCategoryIdRepository, queryCategoryIdRepository);
    }

    @Bean
    CategorySalesService categorySalesService(CategorySalesClient client){
        return new CategorySalesService(client);
    }
}
