package anton.martinez.conrrad.tektonlabsdemo.category.infrastructure.repository;

import anton.martinez.conrrad.tektonlabsdemo.category.domain.Category;
import anton.martinez.conrrad.tektonlabsdemo.category.domain.QueryCategoryIdRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Types;

@Repository
public class QueryCategoryJdbcIdRepository implements QueryCategoryIdRepository {

    private JdbcTemplate jdbcTemplate;
    private CategoryRowMapper categoryRowMapper;

    public QueryCategoryJdbcIdRepository(JdbcTemplate jdbcTemplate, CategoryRowMapper categoryRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.categoryRowMapper = categoryRowMapper;
    }


    @Override
    public Category findById(Integer id) {
        return jdbcTemplate.queryForObject("select * from category where id = ?", new Object[]{id},
                new int[]{Types.NUMERIC}, categoryRowMapper);
    }
}
