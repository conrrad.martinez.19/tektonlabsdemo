package anton.martinez.conrrad.tektonlabsdemo.category.infrastructure.rest;

import anton.martinez.conrrad.tektonlabsdemo.category.application.CategorySalesService;
import anton.martinez.conrrad.tektonlabsdemo.category.domain.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequestMapping(value = "/category")
@RestController
public class CategoryAggregateRestController {

    private CategoryProductsAggregateFactory categoryProductsAggregateFactory;
    private QueryCategoryIdRepository queryCategoryIdRepository;
    private QueryCategoryRepository queryCategoryRepository;
    private CategorySalesService categorySalesService;
    public CategoryAggregateRestController(CategoryProductsAggregateFactory categoryProductsAggregateFactory,
                                           QueryCategoryIdRepository queryCategoryIdRepository,
                                           QueryCategoryRepository queryCategoryRepository,
                                           CategorySalesService categorySalesService) {
        this.categoryProductsAggregateFactory = categoryProductsAggregateFactory;
        this.queryCategoryIdRepository = queryCategoryIdRepository;
        this.queryCategoryRepository = queryCategoryRepository;
        this.categorySalesService = categorySalesService;
    }

    @GetMapping("/{id}/products")
    public Flux<CategoryProductsAggregate> findProductsById(@PathVariable Integer id) {
        return categorySalesService.addSalesToCategory(categoryProductsAggregateFactory.newInstance(id));
    }

    @GetMapping("/{id}")
    public Mono<Category> findCategoryById(@PathVariable Integer id) {
        return Mono.just(queryCategoryIdRepository.findById(id));
    }

    @GetMapping
    public Flux<Category> findAllCategories(){
        return Flux.fromStream(queryCategoryRepository.findAll().stream());
    }
}
