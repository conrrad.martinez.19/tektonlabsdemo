package anton.martinez.conrrad.tektonlabsdemo.product.infrastructure.rest;

import anton.martinez.conrrad.tektonlabsdemo.product.domain.Product;
import anton.martinez.conrrad.tektonlabsdemo.product.domain.repository.QueryProductRepository;
import anton.martinez.conrrad.tektonlabsdemo.product.domain.repository.SaveCommandProductRepository;
import anton.martinez.conrrad.tektonlabsdemo.product.domain.repository.UpdateProductCommandRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RequestMapping(value = "/product")
@RestController
public class ProductRestController {

    private QueryProductRepository queryProductRepository;
    private SaveCommandProductRepository saveCommandProductRepository;
    private UpdateProductCommandRepository updateProductCommandRepository;


    public ProductRestController(QueryProductRepository queryProductRepository,
                                 SaveCommandProductRepository saveCommandProductRepository,
                                 UpdateProductCommandRepository updateProductCommandRepository) {
        this.queryProductRepository = queryProductRepository;
        this.saveCommandProductRepository = saveCommandProductRepository;
        this.updateProductCommandRepository = updateProductCommandRepository;
    }

    @PostMapping
    public ResponseEntity<Mono<Void>> addProduct(@RequestBody Product product) {
        saveCommandProductRepository.insert(product);
        return ResponseEntity.noContent().build();
    }

    @PutMapping
    public Mono<Integer> updateProduct(@RequestBody Product product) {
        return Mono.just(updateProductCommandRepository.update(product));
    }

    @GetMapping("/id/{idProduct}")
    public Mono<Product> findById(@PathVariable Integer idProduct) {
        return Mono.just(queryProductRepository.findById(idProduct));
    }
}
