package anton.martinez.conrrad.tektonlabsdemo.product.domain.repository;

import anton.martinez.conrrad.tektonlabsdemo.product.domain.Product;

public interface QueryProductRepository {

    Product findById(Integer productId);
}
