package anton.martinez.conrrad.tektonlabsdemo.product.infrastructure.repository;

import anton.martinez.conrrad.tektonlabsdemo.product.domain.Product;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ProductRowMapper implements RowMapper<Product> {
    @Override
    public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
        Product.ProductBuilder builder = new Product.ProductBuilder();
        return builder.id(rs.getInt("id"))
                .name(rs.getString("name"))
                .price(rs.getBigDecimal("price"))
                .categoryId(rs.getInt("categoryId")).build();
    }
}
