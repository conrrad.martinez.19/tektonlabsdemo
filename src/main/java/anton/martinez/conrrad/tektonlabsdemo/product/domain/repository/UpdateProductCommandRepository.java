package anton.martinez.conrrad.tektonlabsdemo.product.domain.repository;

import anton.martinez.conrrad.tektonlabsdemo.product.domain.Product;

public interface UpdateProductCommandRepository {

    Integer update(Product product);
}
