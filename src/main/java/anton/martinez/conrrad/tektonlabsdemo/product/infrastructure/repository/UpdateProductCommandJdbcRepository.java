package anton.martinez.conrrad.tektonlabsdemo.product.infrastructure.repository;

import anton.martinez.conrrad.tektonlabsdemo.product.domain.Product;
import anton.martinez.conrrad.tektonlabsdemo.product.domain.repository.UpdateProductCommandRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Types;

@Repository
public class UpdateProductCommandJdbcRepository implements UpdateProductCommandRepository {

    private JdbcTemplate jdbcTemplate;

    public UpdateProductCommandJdbcRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Integer update(Product product) {
        return jdbcTemplate.update("update product set name = ?, price = ?, categoryId = ? where id = ?",
                new Object[]{product.getName(), product.getPrice(), product.getCategoryId(), product.getId()},
                new int[]{Types.VARCHAR, Types.DECIMAL, Types.NUMERIC, Types.NUMERIC});
    }
}
