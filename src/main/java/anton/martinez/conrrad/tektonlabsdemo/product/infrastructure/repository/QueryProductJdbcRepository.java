package anton.martinez.conrrad.tektonlabsdemo.product.infrastructure.repository;

import anton.martinez.conrrad.tektonlabsdemo.product.domain.Product;
import anton.martinez.conrrad.tektonlabsdemo.product.domain.repository.QueryProductRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Types;

@Repository
public class QueryProductJdbcRepository implements QueryProductRepository {

    private JdbcTemplate jdbcTemplate;
    private ProductRowMapper productRowMapper;

    public QueryProductJdbcRepository(JdbcTemplate jdbcTemplate, ProductRowMapper productRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.productRowMapper = productRowMapper;
    }

    @Override
    public Product findById(Integer productId) {
        return jdbcTemplate.queryForObject("select * from product where id = ?", new Object[]{productId},
                new int[]{Types.NUMERIC}, productRowMapper);
    }
}
