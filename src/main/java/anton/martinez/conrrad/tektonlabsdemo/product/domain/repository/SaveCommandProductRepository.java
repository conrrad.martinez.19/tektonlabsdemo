package anton.martinez.conrrad.tektonlabsdemo.product.domain.repository;

import anton.martinez.conrrad.tektonlabsdemo.product.domain.Product;

public interface SaveCommandProductRepository {

    void insert(Product product);

}
