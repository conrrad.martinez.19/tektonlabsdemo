package anton.martinez.conrrad.tektonlabsdemo.product.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

public class Product implements Serializable {

    private Integer id;

    private String name;

    private BigDecimal price;

    private Integer categoryId;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public static class ProductBuilder {
        private Product wrapper;

        public ProductBuilder() {
            wrapper = new Product();
        }

        public ProductBuilder id(Integer id) {
            wrapper.id = id;
            return this;
        }

        public ProductBuilder name(String name) {
            wrapper.name = name;
            return this;
        }

        public ProductBuilder price(BigDecimal price) {
            if(Objects.isNull(price)) {
                throw new ProductException("Debes establecer un precio al producto");
            }
            wrapper.price = price;
            return this;
        }

        public ProductBuilder categoryId(Integer categoryId) {
            wrapper.categoryId = categoryId;
            return this;
        }

        public Product build(){
            return wrapper;
        }

    }
}
