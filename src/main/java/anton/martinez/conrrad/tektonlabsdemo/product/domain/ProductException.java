package anton.martinez.conrrad.tektonlabsdemo.product.domain;

public class ProductException extends RuntimeException {
    public ProductException(String s) {
        super(s);
    }
}
