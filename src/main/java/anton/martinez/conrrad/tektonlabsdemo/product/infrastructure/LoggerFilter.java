package anton.martinez.conrrad.tektonlabsdemo.product.infrastructure;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

@Component
public class LoggerFilter implements WebFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerFilter.class);

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        final long time = System.currentTimeMillis();
        return chain.filter(exchange)
                .doFinally(se -> LOGGER.info("Api Respose Time {}: {} ms ", exchange.getRequest().getURI(),
                System.currentTimeMillis() - time));
    }
}
