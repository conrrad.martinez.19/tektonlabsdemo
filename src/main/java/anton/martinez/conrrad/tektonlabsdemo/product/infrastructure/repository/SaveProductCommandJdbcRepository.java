package anton.martinez.conrrad.tektonlabsdemo.product.infrastructure.repository;

import anton.martinez.conrrad.tektonlabsdemo.product.domain.Product;
import anton.martinez.conrrad.tektonlabsdemo.product.domain.repository.SaveCommandProductRepository;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Types;

@Repository
public class SaveProductCommandJdbcRepository implements SaveCommandProductRepository {

    private SimpleJdbcInsert simpleJdbcInsert;

    public SaveProductCommandJdbcRepository(DataSource dataSource) {
        this.simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("product")
                .usingColumns("id", "name", "price", "categoryId");
    }

    @Override
    public void insert(Product product) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", product.getId(), Types.NUMERIC);
        parameterSource.addValue("name", product.getName(), Types.VARCHAR);
        parameterSource.addValue("price", product.getPrice(), Types.DECIMAL);
        parameterSource.addValue("categoryId", product.getCategoryId(), Types.NUMERIC);
        simpleJdbcInsert.execute(parameterSource);
    }
}
