create table if not exists category (
    id numeric identity not null PRIMARY KEY,
    name varchar(100) not null,
    description varchar(250) not null
);
create table if not exists product (
    id numeric identity not null PRIMARY KEY,
    name varchar(64) not null,
    price decimal not null,
    categoryId numeric not null references category(id)
);