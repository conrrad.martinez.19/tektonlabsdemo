insert into category(id,name,description) values (1,'Tubercúlos', 'Productos de cocecha'),
(2,'Legumbres', 'Productos de menestras'),
(3,'Cereales', 'Productos de granos'),
(4,'Fruta', 'Productos organicos');

insert into product(id,name,price, categoryId) values (1,'Arroz', 3.42, 3),
(2,'Quinua', 10, 3),
(3,'Papa Chancai', 1.10, 1),
(4,'Yuca', 0.60, 1),
(5,'Camote', 0.40, 1),
(6, 'Lentejas', 4.1, 2),
(7, 'Manzana', 1.2, 4);